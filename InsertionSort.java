import java.util.Scanner;

class InsertionSort
{
  public static void main(String args[])
  {
    int i, j, n, key, a[];

    Scanner in = new Scanner(System.in);
    System.out.println("Enter the number of elements of array");
    n = in.nextInt();
    a = new int[n];

    System.out.println("Enter " + n + " integers");

    for (i = 0; i < n; i++)
      a[i] = in.nextInt();


		for ( j = 1; j < a.length; j++ ) {
			key = a[j];
			i = j;

			while ( i > 0 && a[i-1] > key ) {
				a[i] = a[i-1];
				i = i - 1;
				a[i] = key;
			}

		}
		for( i = 0; i < a.length; i++)
			System.out.print(a[i] + " ");
			System.out.println("");

	}
}